DDL -> Data Definition Language
--CREATE
--untuk create database
CREATE DATABASE nama_database
CREATE DATABASE TrainingSQLDotnet

--untuk use database yang ada
USE TrainingSQLDotnet

-- Syntax create table = CREATE TABLE nama_table(isi)
CREATE TABLE EmployeeRole(
	EmployeeRoleId INT PRIMARY KEY IDENTITY(1,1),
	Role Varchar(255) NOT NULL
)

CREATE TABLE Employee(
	EmployeeId INT PRIMARY KEY IDENTITY(1,1),
	EmployeeRoleId INT FOREIGN KEY REFERENCES EmployeeRole(EmployeeRoleId),
	EmployeeName VARCHAR(255) NOT NULL,
	EmployeePhone VARCHAR(255) NOT NULL,
	EmployeeSalary INT NOT NULL CONSTRAINT check_salary CHECK(EmployeeSalary>1000000),
	EmployeeRating DECIMAL(4,2),
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'System',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'System',
)

CREATE TABLE Employee2(
	EmployeeId INT PRIMARY KEY IDENTITY(1,1),
	EmployeeRoleId INT FOREIGN KEY REFERENCES EmployeeRole(EmployeeRoleId),
	EmployeeName VARCHAR(255) NOT NULL,
	EmployeePhone VARCHAR(255) NOT NULL,
	EmployeeSalary INT NOT NULL CONSTRAINT check_salary2 CHECK(EmployeeSalary>1000000),
	EmployeeRating DECIMAL(4,2),
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'System',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'System',
)

CREATE TABLE EmployeePerformance(
	EmployeePerformanceId INT PRIMARY KEY IDENTITY(1,1),
	EmployeeId INT FOREIGN KEY REFERENCES Employee(EmployeeId) ON DELETE CASCADE,
	Score INT,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'System',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'System',
)

create table new_data_user(
	userid varchar(255) primary key,
	username varchar(255)
)
insert into datauser values (2,'ABC')
insert into new_data_user values('User002','ABCDEFG')
-- 1..* , *..*

create table unique_user(
	userId UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
	username varchar(255)
)
-- di C# namanya GUID

insert into unique_user(username) values ('User yang unik')

select * From unique_user

select * From datauser

--untuk relasi many to many butuh table mapping
CREATE TABLE VideoAdsMapping(
	VideoAdsMapping INT PRIMARY KEY IDENTITY(1,1),
	VideoId INT FOREIGN KEY REFERENCES Video(VideoId),
	AdsId INT FOREIGN KEY REFERENCES Ads(AdsId)
)

insert into EmployeePerformance(EmployeeId,score) values (2,100)

--ALTER -> syntax buat update table kalian
-- buat tambah kolom
ALTER TABLE UjiCoba
ADD kolom_2 int

-- buat delete kolom
ALTER TABLE UjiCoba
DROP COLUMN kolom_2
select * from UjiCoba

--Untuk ubah tipe data dari sebuah kolom
ALTER TABLE UjiCoba
ALTER COLUMN kolom_2 varchar(255)

--DROP
--drop table
drop table UjiCoba
--drop database
drop database TrainingSQLDotnet

--TRUNCATE
-- kosongin table secara keseluruhan
truncate table employee

DML -> Data Manipulation Language
--INSERT

--insert dengan value yang kita tentukan secara manual
INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('A',1,'2123421',5000000,2.2)
INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('B',1,'226354',5000000,2.2)
INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('C',1,'762342',5000000,2.2)

INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('A',2,'2123421',6000000,2.2)
INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('E',2,'226354',6000000,2.2)
INSERT INTO 
Employee(EmployeeName,EmployeeRoleId,EmployeePhone,EmployeeSalary,EmployeeRating)
Values ('F',2,'762342',7000000,2.2)



--insert dari table lain
INSERT INTO
Employee2(EmployeeName,EmployeePhone,EmployeeRating,EmployeeSalary) 
SELECT 
	EmployeeName,
	EmployeePhone,
	EmployeeRating,
	EmployeeSalary 
from Employee e 
where 
not exists(SELECT 
EmployeeName,EmployeePhone,EmployeeRating,EmployeeSalary 
from Employee2 where EmployeeName in(e.EmployeeName) )

--SELECT
SELECT EmployeeName as [Nama Karyawan] FROM Employee 

select * from Employee where EmployeeName like ('%be%')

select * from Employee where EmployeeName between 'Crist' and 'Roberto' order by EmployeeName desc

select 
	max(EmployeeRating) as [Max Rating], 
	min(EmployeeRating) as [Min Rating],
	sum(EmployeeRating) as [Total Rating],
	count(EmployeeRating) as [Total Data],
	AVG(EmployeeRating) as [Rata-rata] 
from Employee

SELECT 
e.EmployeeName,
er.Role,
ep.Score
FROM EmployeeRole er
LEFT JOIN Employee e on e.EmployeeRoleId = er.EmployeeRoleId
LEFT JOIN EmployeePerformance ep on e.EmployeeId = ep.EmployeeId

select * from EmployeeRole
-- JOIN
-- Inner Join
-- Left Join
-- Right join

delete Employee
--UPDATE

select top 1 with ties EmployeeSalary From Employee order by EmployeeSalary

--5000000 1
--5000000 2
--5000000 3
--6000000 4 
--6000000 ties
--6000000 ties
--6000000 ties

update Employee 
set EmployeeName = er.RoleName,EmployeeRoleId = 3 
from EmployeeRole er 
where er.EmployeeRoleId = Employee.EmployeeRoleId
--DELETE

select * from EmployeeRole

SQL Data Type
--int 1 2 3 4 6  100000
--char nama char(100)
--varchar nama varchar(100) 
--decimal harga decimal(10,2)
--bit -> boolean sql 1/0
--time hh:mm:ss
--date YYYY-MM-DD
--datetime2 YYYY-MM-DD hh:mm:ss
--datetimeoffset YYYY-MM-DD hh:mm:ss

select 
 case 
 when EmployeeRating is not null then 'Tidak ada data rating'
 when EmployeeSalary = 6000000 then 'Gajinya 6 juta'
 else 'Gajinya tidak terhitung'
 end as 'Deskripsi'
from Employee

update Employee set EmployeeRating = null where EmployeeId = 22


--ProductCategory
--Product
--Admin
--Customer
--Transaction
--TransactionDetail

CREATE DATABASE Restaurant
USE Restaurant

CREATE TABLE ProductCategory(
	ProductCategoryId INT PRIMARY KEY IDENTITY(1,1),
	ProductCategoryName VARCHAR(255) NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)

CREATE TABLE Product(
	ProductId INT PRIMARY KEY IDENTITY(1,1),
	ProductCategoryId INT FOREIGN KEY REFERENCES ProductCategory(ProductCategoryId),
	ProductName VARCHAR(255) NOT NULL,
	Price DECIMAL(10,2) NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)


CREATE TABLE Admin(
	AdminId INT PRIMARY KEY IDENTITY(1,1),
	[Name] VARCHAR(255) NOT NULL,
	EmployeedAt DATETIMEOFFSET NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)

CREATE TABLE Customer(
	CustomerId INT PRIMARY KEY IDENTITY(1,1),
	[Name] VARCHAR(255) NOT NULL,
	Phone VARCHAR(16) NOT NULL,
	Address Varchar(255) NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)

CREATE TABLE CustomerTransaction(
	TransactionId INT PRIMARY KEY IDENTITY(1,1),
	CustomerId INT FOREIGN KEY REFERENCES Customer(CustomerId) NOT NULL,
	AdminId INT FOREIGN KEY REFERENCES Admin(AdminId) NOT NULL,
	Total DECIMAL(10,2) NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)

CREATE TABLE CustomerTransactionDetail(
	TransactionDetailId INT PRIMARY KEY IDENTITY(1,1),
	TransactionId INT FOREIGN KEY REFERENCES CustomerTransaction(TransactionId) NOT NULL,
	ProductId INT FOREIGN KEY REFERENCES Product(ProductId) NOT NULL,
	Quantity INT NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',
	UpdatedAt DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM',
)


CREATE TABLE NewProduct(
	ProductId INT PRIMARY KEY IDENTITY(1,1),
	Quantity INT 
)

CREATE TABLE Trans(
	TransId INT PRIMARY KEY IDENTITY(1,1),
	ProductId INT FOREIGN KEY REFERENCES NewProduct(ProductId),
	Quantity INT Constraint check_Stock Check(Quantity<(
	Select Quantity from NewProduct where ProductId = ProductId)
	)
)