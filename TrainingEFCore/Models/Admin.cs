﻿using System;
using System.Collections.Generic;

namespace TrainingEFCore.Models
{
    public partial class Admin
    {
        public Admin()
        {
            CustomerTransaction = new HashSet<CustomerTransaction>();
        }

        public int AdminId { get; set; }
        public string Name { get; set; }
        public DateTimeOffset EmployeedAt { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<CustomerTransaction> CustomerTransaction { get; set; }
    }
}
