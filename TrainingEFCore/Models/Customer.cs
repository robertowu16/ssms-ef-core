﻿using System;
using System.Collections.Generic;

namespace TrainingEFCore.Models
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerTransaction = new HashSet<CustomerTransaction>();
        }

        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<CustomerTransaction> CustomerTransaction { get; set; }
    }
}
