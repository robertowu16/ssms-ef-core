﻿using System;
using System.Collections.Generic;

namespace TrainingEFCore.Models
{
    public partial class CustomerTransaction
    {
        public CustomerTransaction()
        {
            CustomerTransactionDetail = new HashSet<CustomerTransactionDetail>();
        }

        public int TransactionId { get; set; }
        public int CustomerId { get; set; }
        public int AdminId { get; set; }
        public decimal Total { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }

        public virtual Admin Admin { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<CustomerTransactionDetail> CustomerTransactionDetail { get; set; }
    }
}
