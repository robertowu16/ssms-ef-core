﻿using System;
using System.Collections.Generic;

namespace TrainingEFCore.Models
{
    public partial class NewProduct
    {
        public int ProductId { get; set; }
        public int? Quantity { get; set; }
    }
}
