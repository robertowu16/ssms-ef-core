﻿using System;
using System.Collections.Generic;

namespace TrainingEFCore.Models
{
    public partial class Product
    {
        public Product()
        {
            CustomerTransactionDetail = new HashSet<CustomerTransactionDetail>();
        }

        public int ProductId { get; set; }
        public int? ProductCategoryId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<CustomerTransactionDetail> CustomerTransactionDetail { get; set; }
    }
}
