﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingEFCore.Models;

namespace TrainingEFCore.Services
{
    public class RestaurantService
    {
        private RestaurantContext DB;

        public RestaurantService(RestaurantContext restaurantContext)
        {
            this.DB = restaurantContext;
        }

        public async Task<List<Product>> GetAllProduct()
        {

            var result = await this.DB.Product.ToListAsync();
            return result;
        }
    }
}
